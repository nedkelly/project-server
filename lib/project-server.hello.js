/**
 * project-server
 * @version v0.4.6
 * @timestamp Thu Nov 22 2018 - 13:53:20 GMT+1100 (Australian Eastern Daylight Time)
 */

"use strict";

const base64      = require('base-64');
const utf8        = require('utf8');
const ip          = require('ip');

/**
 * Hello Module
 * @param  {Object} req
 * @param  {Object} res
 * @param  {String} query
 * @param  {String} rootPath
 * @param  {String} host
 * @return {Void}
 */
let projectServerHello = function(req, res, query, rootPath, host, pkgData) {

  let ipAddress = ip.address(),
      port = host.substr(host.indexOf(':'), host.length);

  let output = "<h1>Hello " + pkgData.author.name.split(' ')[0] + "</h1>";
      output += "<code><pre>Your Project Server IP is @ <strong>" + ipAddress + port + "</strong></pre></code>";
      output += "<h2>Project Package Data</h2>";
      output += "<code><pre>" + JSON.stringify(pkgData, null, 2) + "</pre></code>";

  let html = '<html><head><meta charset="utf-8"><title>Hello!</title></head><style>body{color: #333; font: normal 14px/1.5 "Helvetica Neue", Helvetica, Arial, freesans, sans-serif}code{color: #666}.styleguide{padding:30px}h1{margin: 0 0 16px 0}h2{margin:16px 0}</style><body><div class="styleguide">' + output + '</div></body></html>';
  let bytes = utf8.encode(html);
  let encoded = base64.encode(bytes);
  let body = JSON.stringify('data:text/html;charset=UTF-8;base64,' + encoded);
  let buf = new Buffer(body, 'utf8');

  // Set headers
  res.setHeader('Content-Type', 'application/json; charset=utf-8');
  res.setHeader('Content-Length', buf.length);
  res.setHeader('X-Hello-Status', "Hello has been said");
  res.end(buf);
}

/**
 * Module exports.
 * @public
 */
module.exports = projectServerHello;
