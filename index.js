
"use strict";

/*!
 * Exports
 */
module.exports = {
  projectServerIndex: require('./lib/project-server.index'),
  projectServerStyleguide: require('./lib/project-server.styleguide')
};
