# ![][logo] Project Server

> A [Browsersync](https://www.browsersync.io/) middleware that provides both an elegant Serve Index and a fully decoupled static Styleguide generator

- Designed to provide a fast, flexible development view to the output generated by the build tools of your choice.
- Fully configurable to allow you to present your code your way.
- Completely decoupled from the build tools so that there is no code pollution within the final product, no Styleguide assets are injected into the actual build.

## Install

`npm i -D project-server`

## Usage

To start using Project Server you can simply install it along with [Browsersync](https://www.browsersync.io/) into your favourite build platform and configure it as follows.

> Here are a couple of flavours I prepared earlier, install and run them to see Project Server in action.

* [Gulp Boilerplate](https://bitbucket.org/nedkelly/gulp-boilerplate)
* [Grunt Boilerplate](https://bitbucket.org/nedkelly/grunt-boilerplate)
* [NPM Boilerplate](https://bitbucket.org/nedkelly/npm-boilerplate)

#### General Config

Assuming a tree structure like this (or take a look at the example builds above):

```
.
├── gulpfile.js / gruntfile.js
├── public
    ├── assets
    │   └── ...
    │
    └── docs
    │   ├── document-1.html
    │   ├── document-2.html
    │   └── ...
    │
    ├── index.html
    ├── template-1.html
    ├── template-2.html
    └── ...
```

#### The Configuration Object

``` javascript
var pkg = require('path/to/package.json');

your_config_object: {
  defaultPage: '/docs/README.html',
  favicon: '/assets/img/favicon.jpg',
  localStorageSeed: 'my cat is fat',
  menus: [
    { title: 'Pages', path: '/', accesskey: 'p' },
    {},
    { title: 'Documentation', path: '/docs', accesskey: 'd' },
  ],
  pkg: {
    name: pkg.name,
    version: pkg.version,
  },
  publicDir: './public/',
  relativeDir: '',
  ignorePatterns: /(--[\w]*?--)/g,
  styleguidePage: 'styleguide.html',
};
```

#### defaultPage
**Type:** `String`
**Required:** `false`

The page that will load in the viewport by default if there is no page already saved in `local storage`.

#### favicon
**Type** `String`
**Default** `a picture of my dog`
**Required** `false`

Allows you to set the source of the favicon .

#### localStorageSeed
**Type:** `String`
**Default:** `project-server` and `project-server.styleguide`
**Required:** `false`

A unique seed used to store this instance of the `project-server` against in the browser's `local storage`, because you might be running more than one project or the current URL eg: `http://localhost:3000`

#### menus
**Type:** `Array`
**Required:** `true`

An array of menu items that you want to construct the Styleguide menu from, each item consists of a `title`, a `path` and an optional `accesskey`, this is pretty self-explanatory.

An empty item will result in a visible seperator in the list.

#### menuStartDepth
**Type:** `Integer`
**Required:** `false`

If a `menuStartDepth` of `1` is supplied `project-server` will build menus inside the first level, eg: `/public/folder1/`, where `/public/` is the directory we are serving from but we only want to build menus for it's immediate subdirs.

```
.
├── public
    ├── folder1
    │   ├── assets
    │   ├── docs
    │   ├── index.html
    │   └── ...
    └── folder2
        ├── assets
        ├── docs
        ├── index.html
        └── ...
```

__Note:__ _Only supports the first level deep at this stage, I might add more levels if it's ever needed but can't justify it right now._

#### ignoreDirectories
**Type:** `Array`
**Default** `null`
**Required:** `false`

An optional array of directory names to ignore (eg: don't render them in the menu view) if the `menuStartDepth` is set to `1`. The names must exactly match the directory names.

#### pkg
**Type:** `Object`
**Default:** `No Package Name` and `No Package Version`
**required:** `false`

Allows you to pass `package.name`, `package.version` to the Project Server.

#### publicDir
**Type** `String`
**Default** `./public/`
**Required** `true`

The directory that the styleguide `index.html` file will be created in, this must be defined or the server will fail to start.

#### relativeDir
**Type** `String`
**Default** `/`
**Required** `false`

A relative path that will be appended to the styleguide navigation items, useful if the styleguide is hosted or running on a VM.

For example, if your server's web root directory is `/web/` and you want to generate a styleguide in `/web/static/` you might need to set the path to `/static/` to allow pages to be served correctly.

#### ignorePatterns
**Type** `RegExp`
**Default** `/(__[\s\S]+?__)/g`
**Required** `false`

Any files or directories matching the supplied pattern (or by default, patterns like `/(__[\s\S]+?__)/g` e.g. `__mydir__`) will be excluded from the menu and index, this means it's possible to add static assets such as images, css and javascript to serve in your rendered pages.

#### styleguidePage
**Type:** `String`
**Default:** `index.html`
**Required:** `false`

Sets the output file for the static styleguide generator, this defaults to `index.html` so that it automatically acts as the server index if you drop the static bundle onto a virtual host. Whatever you set this to, the Project Server sidebar will not render this file in either the generated menu or the serve index list, this is done simply to avoid infinite nesting in the iframe.

To view the styleguide page while the server is running you can type `/sg` into the filter input in the sidebar, this will launch the static page in a new window/tab.

### Gulp/NPM Script

``` javascript
browserSync.init({

  // Other Browsersync Options here

  files: './public/**',
  server: {
    baseDir: './public/',
    directory: false,
    middleware: [
      require('project-server').projectServerIndex( './public/', {
        config: your_config_object
      })
    ],
  },
});
```

### Grunt

``` javascript
browserSync: {
  your_target: {
    bsFiles: {
      src: ['./public/**']
    },
    options: {

      // Other Browsersync Options here

      server: {
        baseDir: './public/',
        directory: false,
        middleware: [
          require('project-server').projectServerIndex( './public/', {
            config: your_config_object
          })
        ],
      }
    }
  }
};
```

### Styleguide

Project Server can also generate a portable static Styleguide, the Styleguide has the same layout as the server but without the server index listing or the command functionality of the Filter input.

The Styleguide is generated in the `index.html` file in the root web (eg: `./public/`) directory, this way it serves as the index page when the project is dropped onto a static host. This can be overridden by setting the `styleguidePage` in the `projectServer` configuration object.

To generate the styleguide simply invoke the `projectServerStyleguide` method the same as if it were the `projectServerIndex` method.

```javascript
gulp.task('styleguide', function() {
  return require('project-server').projectServerStyleguide( './public/', {
    config: your_config_object
  });
});
```

#### Commands and Hot keys

In the project Server sidebar, type the following into the filter input to get the associated result:

| Cmd | Action |
| --- | --- |
| `/` | Enter/Exit Command Mode |
| `/sg` | Open the static styleguide in a new window |
| `/hello` | Print server and package information |
| `/reset` | Clear local storage and reset the default options |

While the mouse is over the sidebar you will have access to several hot keys:

| Key | Action |
| --- | --- |
| `esc` | Clear and exit filter |
| `c` | Clear current filter |
| `f` | Jump to filter |
| `s` | Toggle sidebar |
| `n` | Toggle open in new window |
| `i` | Toggle index tab |
| `m` | Toggle menu tab |

#### More to do...

This is a work in progress and there are several outstanding issues I'm working to fix but if you have any issues or suggestions please let me know on my [bitbucket project page](https://bitbucket.org/nedkelly/project-server).

[logo]: data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAABfklEQVRYR+2XjW3CMBCFHxOUDcoG7QbQCcoG7QhsAEwAI7QbtBPQbtANWjaACai+yBc5JiYmGEUITrKUv7s83pn3Lj0djqGkgVsj96id12X+SWIRX+6Y8+/Ya3rBDYo/SxpLshc2YEy+DaAPSZ8eSBkAXjyV9Jpc7rQH3yTNAQKAR0krSf3Tah6dvZH0BIBf1+OjK2RIKBjYZSjUusQNwNUzsO6SgS1i1xWAd0kzE6K6vyHajQ/kDGoixSzzi0GMAZNo/GDi/CEFzDpiRvhAGCjvqgkASVCFT/iBjnO9bby4/EYGcgKgpbgsC/MrIjcDFL53tWlfOEvsMdYWQFvqbwCumwETooVPQ8omZDezGN1sbIupZIoQVZQ3BUC2He8KdQbAhAhpLyOFAegn+UcSkywR+9C4TCHCGk0+fYm24zozyrUviomI3Y1d3gWtOTeAciLiRfSOX4pNmkmdE0BlIvLpBAh2ufQu5mpB3URUfpwe6mkoRIjRQyQhRYgqqf9hvmkuWnNPdQAAAABJRU5ErkJggg==
